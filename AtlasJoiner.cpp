#include <iostream>
#include <string>
#include <dirent.h>
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <algorithm>
#include "tinyxml/tinyxml2.h"
using namespace tinyxml2;

std::string base_path;
int original_len;

void process_directory(std::string directory);
void process_file(std::string file);
void process_entity(struct dirent* entity);

sf::Image* image;
XMLDocument* xml;
XMLElement* nRoot;
unsigned int coord_x;
unsigned int coord_y;
int max_row_height;
unsigned int result_size;

int main(int argc, char** argv) {

	if (argc < 2) {
		std::cout << "AtlasJoiner search by root directory all png files to make an altas image and xml file\n[0] = root directory\n[1] (1024) = final image size" << std::endl;
		return 0;
	}

	if (argc == 2)
		result_size = 1024;
	else
		result_size = atoi(argv[ 2 ]);

	
	std::string total_path = argv[ 1 ];

	std::string current_directory;
	for (int i = total_path.size() - 1; i >= 0; i--) {
		if (total_path[ i ] == '/') {
			current_directory = total_path.substr( i + 1 );
			
			base_path = total_path.substr(0, i + 1);
			original_len = i + 1;
			break;
		}
	}

	coord_x = 0;
	coord_y = 0;
	max_row_height = 0;

	image = new sf::Image();
	image->create(result_size, result_size);

	xml = new XMLDocument();
	nRoot = xml->NewElement("atlas");
	xml->InsertFirstChild(nRoot);

	nRoot->SetAttribute("name", current_directory.c_str());
	std::string path_image_src = current_directory + ".png";
	nRoot->SetAttribute("src", path_image_src.c_str());

    process_directory(current_directory);

    std::string path_result = base_path + current_directory + ".png";
    image->saveToFile(path_result.c_str());

    std::string path_xml = base_path + current_directory + ".xml";
    xml->SaveFile(path_xml.c_str());

    delete image;
	return 0;
}


void process_directory(std::string directory) {

    std::string dirToOpen = base_path + directory;
    auto dir = opendir(dirToOpen.c_str());

    //set the new path for the content of the directory
    base_path = dirToOpen + "/";

    // std::cout << "Process directory: " << dirToOpen.substr(original_len).c_str() << std::endl;

    if (NULL == dir) {
        std::cout << "could not open directory: " << dirToOpen.c_str() << std::endl;
        return;
    }

    auto entity = readdir(dir);

    while (entity != NULL) {
        process_entity(entity);
        entity = readdir(dir);
    }

    //we finished with the directory so remove it from the path
    base_path.resize(base_path.length() - 1 - directory.length());
    closedir(dir);
}

void process_entity(struct dirent* entity) {

    //find entity type
    if (entity->d_type == DT_DIR) {
    	//it's an direcotry

        //don't process the  '..' and the '.' directories
        if(entity->d_name[0] == '.')
            return;

        //it's an directory so process it
        process_directory(std::string(entity->d_name));
        return;
    }

    if(entity->d_type == DT_REG) {
    	//regular file
        process_file(std::string(entity->d_name));
        return;
    }

    //there are some other types
    //read here http://linux.die.net/man/3/readdir
    std::cout << "Not a file or directory: " << entity->d_name << std::endl;
}

void process_file(std::string file) {


	std::string file_path = base_path + file;

	std::string instance_name = file_path.substr(original_len);
    
	sf::Image current_image;
	if ( !current_image.loadFromFile(file_path.c_str())) {
		std::cout << "Error: fail to load " << file_path.c_str() << std::endl;
		return;
	}

	int image_width = current_image.getSize().x;
	int image_height = current_image.getSize().y;

	if (coord_x + image_width > result_size) {
		coord_x = 0;
		coord_y += max_row_height;
		max_row_height = 0;
	}

	if (coord_y + image_height > result_size) {
		std::cout << "Error: result size image is too small to " << instance_name.c_str() << std::endl;
		return;
	}

	max_row_height = std::max(max_row_height, image_height);


	image->copy(current_image, coord_x, coord_y);
	
	XMLElement* instance_element = xml->NewElement("instance");
	instance_element->SetAttribute("name", instance_name.c_str());
	instance_element->SetAttribute("x", coord_x);
	instance_element->SetAttribute("y", coord_y);
	instance_element->SetAttribute("width", image_width);
	instance_element->SetAttribute("height", image_height);
	nRoot->InsertEndChild(instance_element);


	coord_x += image_width;

	std::cout << "put " << instance_name << std::endl;
}

