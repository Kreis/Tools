#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include <memory>
#include "tinyxml/tinyxml2.h"
using namespace tinyxml2;

struct Rect {
	int x;
	int y;
	int w;
	int h;
};

class Resources {
public:
	Resources() {	}

	std::shared_ptr<sf::Sprite> get_instance(std::string name_instance) {
		if (map_rects[ name_instance ] == nullptr) {
			std::cout << "Error: AnimatorPreview doesn't have atlas with instance " << name_instance << std::endl;
			return nullptr;
		}

		std::shared_ptr<sf::Sprite> sprite = std::make_shared<sf::Sprite>();
		sprite->setTexture(*map_textures[ name_instance ].get());

		std::shared_ptr<Rect> rect = map_rects[ name_instance ];
		sprite->setTextureRect(sf::IntRect(rect->x, rect->y, rect->w, rect->h));

		return sprite;
	}

	bool add_atlas(std::string atlas_path) {
		XMLDocument xml;
		XMLError e_result = xml.LoadFile(atlas_path.c_str());
		if (e_result != XML_SUCCESS) {
			std::cout << "Error: fail to load " << atlas_path << " atlas file" << std::endl;
			return false;
		}

		XMLElement* atlas_root = xml.FirstChildElement("atlas");
		const char* src_atlas = atlas_root->Attribute("src");
		if (src_atlas == nullptr) {
			std::cout << "Error: not found sorce image to atlas" << std::endl;
			return false;
		}

		std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>();
		texture->loadFromFile(src_atlas);

		XMLElement* instance = atlas_root->FirstChildElement("instance");
		while (instance != nullptr) {

			const char* _name 	= instance->Attribute("name");
			const char* _x 		= instance->Attribute("x");
			const char* _y 		= instance->Attribute("y");
			const char* _width 	= instance->Attribute("width");
			const char* _height = instance->Attribute("height");

			if (_name == nullptr || _x == nullptr || _y == nullptr || _width == nullptr || _height == nullptr) {
				std::cout << "Error: instance with corrupted attributes" << std::endl;
				return false;
			}
			
			std::string name_instance = _name;

			std::shared_ptr<Rect> current_rect = std::make_shared<Rect>();
			current_rect->x = atoi(_x);
			current_rect->y = atoi(_y);
			current_rect->w = atoi(_width);
			current_rect->h = atoi(_height);

			map_rects[ name_instance ] = current_rect;
			map_textures[ name_instance ] = texture;

			instance = instance->NextSiblingElement("instance");
		}
		return true;
	}

private:
	std::map<std::string, std::shared_ptr<sf::Texture> > map_textures;
	std::map<std::string, std::shared_ptr<Rect> > map_rects;
};

class Animation {
public:
	Animation(std::string name, Resources& res) {
		this->name = name;
		this->res = &res;

		delay = 1 / 60;
		loop_at = 0;
		current_frame = 0;
		current_time = 0;
	}

	void restart() {
		current_time = 0;
		current_frame = 0;
	}

	void put_fps(double fps) {
		delay = 1 / fps;
	}
	void put_loop_at(int loop_at) {
		this->loop_at = loop_at;
	}

	void add_frame(std::string name_instance, int x, int y) {
		std::shared_ptr<sf::Sprite> sprite = res->get_instance(name_instance);
		vector_frames.push_back(sprite);
		vector_frame_x.push_back(x);
		vector_frame_y.push_back(y);
	}

	void update(double dt) {
		current_time += dt;
		if (current_time < delay)
			return;

		current_time = 0;

		bool last_frame = current_frame == vector_frames.size() - 1;
		if ( ! last_frame) {
			current_frame++;
			return;
		}

		if (loop_at < 0)
			loop_at = vector_frames.size() - 1;

		current_frame = loop_at;
	}

	void draw(sf::RenderWindow& window) {
		window.setTitle(name);

		std::shared_ptr<sf::Sprite> sprite = vector_frames[ current_frame ];
		int x = vector_frame_x[ current_frame ];
		int y = vector_frame_y[ current_frame ];
		sprite->setPosition(x, y);
		window.draw(*sprite.get());
	}

private:
	double current_time;
	int current_frame;
	double delay;
	int loop_at;
	std::vector<std::shared_ptr<sf::Sprite> > vector_frames;
	std::vector<int> vector_frame_x;
	std::vector<int> vector_frame_y;
	std::string name;
	Resources* res;
};

class Animator {
public:
	Animator() {
		current_animation = 0;
	}

	bool load(std::string path, Resources& res) {

	 	XMLDocument xml;
		xml.LoadFile(path.c_str());

		XMLError e_result = xml.LoadFile(path.c_str());
		if (e_result != XML_SUCCESS) {
			std::cout << "Error: fail to load " << path << " animator file" << std::endl;
			return false;
		}

		XMLElement* animator_root = xml.FirstChildElement("animator");
		if (animator_root == nullptr) {
			std::cout << "Error: animator xml out of format" << std::endl;
			return false;
		}

		XMLElement* animation_element = animator_root->FirstChildElement("animation");

		while (animation_element != nullptr) {

			const char* _name = animation_element->Attribute("name");
			const char* _fps = animation_element->Attribute("fps");
			const char* _loop_at = animation_element->Attribute("loopAt");

			if (_name == nullptr) {
				std::cout << "Error: animator xml out of format" << std::endl;
				return false;
			}

			int fps 	= _fps == nullptr 		? 60 	: atoi(_fps);
			int loop_at = _loop_at == nullptr 	? 0		: atoi(_loop_at);

			std::shared_ptr<Animation> animation = std::make_shared<Animation>(_name, res);
			animation->put_fps(fps);
			animation->put_loop_at(loop_at);

			XMLElement* instance_element = animation_element->FirstChildElement("instance");

			while (instance_element != nullptr) {
				const char* _name_instance = instance_element->Attribute("name");
				const char* _x_instance = instance_element->Attribute("x");
				const char* _y_instance = instance_element->Attribute("y");

				if (_name_instance == nullptr) {
					std::cout << "Error: animator xml out of format" << std::endl;
					return false;
				}

				int x_instance = _x_instance == nullptr ? 0 : atoi(_x_instance);
				int y_instance = _y_instance == nullptr ? 0 : atoi(_y_instance);

				animation->add_frame(_name_instance, x_instance, y_instance);

				instance_element = instance_element->NextSiblingElement("instance");
			}
			vector_animation.push_back(animation);

			animation_element = animation_element->NextSiblingElement("animation");
		}

		return true;
	}

	void add_animation(std::shared_ptr<Animation> animation) {
		vector_animation.push_back(animation);
	}

	void change_animation(int v) {
		current_animation += v;
		if (current_animation < 0)
			current_animation = vector_animation.size() - 1;
		if (current_animation >= vector_animation.size())
			current_animation = 0;

		vector_animation[ current_animation ]->restart();
	}

	void update(double dt) {
		if (current_animation < 0 || current_animation >= vector_animation.size()) {
			std::cout << "Error: Animator current_animation is out of limits" << std::endl;
			return;
		}

		vector_animation[ current_animation ]->update(dt);
	}
	void draw(sf::RenderWindow& window) {
		if (current_animation < 0 || current_animation >= vector_animation.size()) {
			std::cout << "Error: Animator current_animation is out of limits" << std::endl;
			return;
		}		

		vector_animation[ current_animation ]->draw(window);
	}

private:
	int current_animation;
	std::vector<std::shared_ptr<Animation> > vector_animation;
};

int main (int argc, char** argv) {

	if (argc < 5) {
		std::cout << "AnimatorPreview show a preview of animations by a XML file in format:" << std::endl;
		std::cout << "<animator>" << std::endl;
		std::cout << "  <animation name=\"animation name\" fps=60=\"60\" loopAt=0=\"0\" >" << std::endl;
		std::cout << "    <instance name=\"name of instance by AtlasJoiner\" x=0=\"0\" y=0=\"0\" />" << std::endl;
		std::cout << "  </animation>" << std::endl;
		std::cout << "  <animation name=\"second animation\" fps=60=\"30\" loopAt=0=\"3\" >" << std::endl;
		std::cout << "    <instance name=\"name of instance by AtlasJoiner\" x=0=\"15\" y=0=\"10\" />" << std::endl;
		std::cout << "  </animation>" << std::endl;
		std::cout << "</animator>" << std::endl;

		std::cout << "loopAt=0=\"0\" <- an example that how an attribute has a default value, is optional put it in file" << std::endl;
		std::cout << "\n[ 0 ] = path of xml file" << std::endl;
		std::cout << "[ 1 ] = preview window width" << std::endl;
		std::cout << "[ 2 ] = preview window height" << std::endl;
		std::cout << "[ 3 .. n ] = path of xml generated by AtlasJoiner that will be use for this animator" << std::endl;
		return 0;
	}

	std::string path_animator = argv[ 1 ];
	int width = atoi(argv[ 2 ]);
	int height = atoi(argv[ 3 ]);

	std::vector<std::string> vector_atlas;
	for (int i = 4; i < argc; i++) {
		vector_atlas.push_back(argv[ i ]);
	}

	Resources res;
	for (int i = 0; i < vector_atlas.size(); i++) {
		res.add_atlas(vector_atlas[ i ]);
	}

	Animator animator;
	animator.load(path_animator, res);

	sf::RenderWindow window(sf::VideoMode(width, height), "AnimatorPreview");
	sf::Event ev;

	sf::Clock clock;
	sf::Time time;

	bool up = false;
	bool down = false;
	bool up_flag = false;
	bool down_flag = false;
	while (true) {

		up = false;
		down = false;

		while (window.pollEvent(ev)) {
			if (ev.type == sf::Event::Closed) {
				window.close();
				break;
			}

			if (ev.type == sf::Event::KeyPressed) {
				if (ev.key.code == sf::Keyboard::Up && ! up_flag) {
					up = true;
					up_flag = true;
				}
				if (ev.key.code == sf::Keyboard::Down && ! down_flag) {
					down = true;
					down_flag = true;
				}
			}
			if (ev.type == sf::Event::KeyReleased) {
				if (ev.key.code == sf::Keyboard::Up)
					up_flag = false;
				if (ev.key.code == sf::Keyboard::Down)
					down_flag = false;
			}
		}

		if ( ! window.isOpen())
			break;

		if (up) {
			animator.change_animation(-1);
		}
		if (down) {
			animator.change_animation(1);
		}

 		time = clock.restart();
 		double dt = time.asSeconds();

 		animator.update(dt);

 		window.clear();
 		animator.draw(window);
 		window.display();
	}




	return 0;
}
















